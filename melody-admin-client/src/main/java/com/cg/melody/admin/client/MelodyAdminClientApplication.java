package com.cg.melody.admin.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName: MelodyAdminClientApplication
 * @Description: TODO
 * @author: cao_gang
 * @Date: 2020/3/25 13:18
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MelodyAdminClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(MelodyAdminClientApplication.class,args);
    }
}
