package com.cg.melody.admin.server;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName: MelodyAdminServerApplication
 * @Description: TODO
 * @author: cao_gang
 * @Date: 2020/3/25 10:54
 */
@SpringBootApplication
@EnableAdminServer
@EnableDiscoveryClient
public class MelodyAdminServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(MelodyAdminServerApplication.class, args);
    }

}
