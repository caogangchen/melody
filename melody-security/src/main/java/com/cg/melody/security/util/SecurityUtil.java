package com.cg.melody.security.util;

/**
 * @ClassName: SecurityUtil
 * @Description: Security工具类
 * @author: cao_gang
 * @Date: 2020/3/26 13:20
 */

import com.cg.melody.security.entity.SelfUserEntity;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    /**
     * 私有化构造器
     */
    private SecurityUtil() {
    }

    /**
     * 获取当前用户信息
     */
    public static SelfUserEntity getUserInfo() {
        SelfUserEntity userDetails = (SelfUserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }

    /**
     * 获取当前用户ID
     */
    public static Long getUserId() {
        return getUserInfo().getUserId();
    }

    /**
     * 获取当前用户账号
     */
    public static String getUserName() {
        return getUserInfo().getUsername();
    }
}
