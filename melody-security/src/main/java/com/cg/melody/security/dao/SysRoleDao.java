package com.cg.melody.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.melody.security.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @InterfaceName: SysRoleDao
 * @Description: 角色Dao
 * @author: cao_gang
 * @Date: 2020/3/26 13:43
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
}
