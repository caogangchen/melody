package com.cg.melody.security.controller;

import com.cg.melody.security.util.ResultUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: IndexController
 * @Description: 初始页面
 * @author: cao_gang
 * @Date: 2020/3/26 13:54
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    /**
     * 首页
     * @return
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Map<String, Object> userLogin() {
        // 组装参数
        Map<String, Object> result = new HashMap<>();
        result.put("title", "这里是首页不需要权限和登录拦截");
        return ResultUtil.resultSuccess(result);
    }

}

