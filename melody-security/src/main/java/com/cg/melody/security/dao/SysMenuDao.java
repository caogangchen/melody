package com.cg.melody.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.melody.security.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @InterfaceName: SysMenuDao
 * @Description: 权限DAO
 * @author: cao_gang
 * @Date: 2020/3/26 13:42
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {
}
