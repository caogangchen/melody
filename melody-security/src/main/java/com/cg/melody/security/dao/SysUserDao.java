package com.cg.melody.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.melody.security.entity.SysMenuEntity;
import com.cg.melody.security.entity.SysRoleEntity;
import com.cg.melody.security.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @InterfaceName: SysUserDao
 * @Description: 系统用户DAO
 * @author: cao_gang
 * @Date: 2020/3/26 13:45
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

    /**
     * 通过用户ID查询角色集合
     * @Author Sans
     * @CreateTime 2019/9/18 18:01
     * @Param  userId 用户ID
     * @Return List<SysRoleEntity> 角色名集合
     */
    List<SysRoleEntity> selectSysRoleByUserId(Long userId);
    /**
     * 通过用户ID查询权限集合
     * @Author Sans
     * @CreateTime 2019/9/18 18:01
     * @Param  userId 用户ID
     * @Return List<SysMenuEntity> 角色名集合
     */
    List<SysMenuEntity> selectSysMenuByUserId(Long userId);

}
