package com.cg.melody.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.melody.security.entity.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @InterfaceName: SysRoleMenuDao
 * @Description: 角色权限关系Dao
 * @author: cao_gang
 * @Date: 2020/3/26 13:44
 */
@Mapper
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenuEntity> {
}
