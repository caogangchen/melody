package com.cg.melody.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cg.melody.security.entity.SysMenuEntity;

/**
 * @ClassName: SysMenuService
 * @Description: 权限业务接口
 * @author: cao_gang
 * @Date: 2020/3/26 13:34
 */
public interface SysMenuService extends IService<SysMenuEntity> {

}
