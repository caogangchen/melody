package com.cg.melody.security.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cg.melody.security.dao.SysRoleMenuDao;
import com.cg.melody.security.entity.SysRoleMenuEntity;
import com.cg.melody.security.service.SysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SysRoleMenuServiceImpl
 * @Description: 角色与权限业务实现
 * @author: cao_gang
 * @Date: 2020/3/26 13:47
 */
@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {

}
