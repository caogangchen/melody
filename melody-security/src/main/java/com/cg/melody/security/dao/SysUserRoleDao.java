package com.cg.melody.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.melody.security.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @InterfaceName: SysUserRoleDao
 * @Description: 用户与角色关系DAO
 * @author: cao_gang
 * @Date: 2020/3/26 13:46
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {

}

