package com.cg.melody.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cg.melody.security.entity.SysRoleMenuEntity;

/**
 * @ClassName: SysRoleMenuService
 * @Description: 角色与权限业务接口
 * @author: cao_gang
 * @Date: 2020/3/26 13:35
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

}
