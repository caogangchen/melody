package com.cg.melody.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cg.melody.security.entity.SysUserRoleEntity;

/**
 * @ClassName: SysUserRoleService
 * @Description: 用户与角色业务接口
 * @author: cao_gang
 * @Date: 2020/3/26 13:36
 */

public interface SysUserRoleService extends IService<SysUserRoleEntity> {

}
