package com.cg.melody.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName: SecurityApplication
 * @Description: TODO
 * @author: cao_gang
 * @Date: 2020/3/25 13:23
 */
@MapperScan(basePackages = {"com.cg.melody.security.dao"})
@EnableDiscoveryClient
@SpringBootApplication
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class,args);
    }
}
