package com.cg.melody.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cg.melody.security.dao.SysMenuDao;
import com.cg.melody.security.entity.SysMenuEntity;
import com.cg.melody.security.service.SysMenuService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SysMenuServiceImpl
 * @Description: 权限业务实现
 * @author: cao_gang
 * @Date: 2020/3/26 13:39
 */
@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {

}
