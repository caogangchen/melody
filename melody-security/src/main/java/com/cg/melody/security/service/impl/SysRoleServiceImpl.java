package com.cg.melody.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cg.melody.security.dao.SysRoleDao;
import com.cg.melody.security.entity.SysRoleEntity;
import com.cg.melody.security.service.SysRoleService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SysRoleServiceImpl
 * @Description: 角色业务实现
 * @author: cao_gang
 * @Date: 2020/3/26 13:49
 */
@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleEntity> implements SysRoleService {

}
